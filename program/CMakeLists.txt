cmake_minimum_required(VERSION 3.14)
project(namdVO2 CXX)

include(FetchContent)
FetchContent_Declare(inq
  GIT_REPOSITORY https://gitlab.com/npneq/inq.git  # git@gitlab.com:npneq/inq.git
  GIT_TAG master  # it's much better to use a specific Git revision or Git tag for reproducibility
)

FetchContent_MakeAvailable(inq)

# your program
#add_executable(myprog main.f90)
#target_link_libraries(myprog mylib)  # mylib is from "child"
